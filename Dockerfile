FROM openjdk:11.0.8-jre-slim
COPY build/BikeRental-0.0.1.jar bikeRental.jar
ENTRYPOINT ["java", "-jar", "bikeRental.jar"]

