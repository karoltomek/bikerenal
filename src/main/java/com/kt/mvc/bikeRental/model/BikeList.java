package com.kt.mvc.bikeRental.model;

import java.util.List;

public class BikeList {

    private final Bike bike1 = new Bike(Brand.ROMET, Colour.BLUE, 10, false);
    private final Bike bike2 = new Bike(Brand.SCOT, Colour.BLACK, 20, false);
    private final Bike bike3 = new Bike(Brand.TREK, Colour.GRAY, 15, true);
    private final Bike bike4 = new Bike(Brand.KROSS, Colour.GREEN, 30, false);
    private final Bike bike5 = new Bike(Brand.ROMET, Colour.RED, 12, false);
    private final Bike bike6 = new Bike(Brand.KROSS, Colour.WHITE, 11, true);
    private final Bike bike7 = new Bike(Brand.GIANT, Colour.YELLOW, 40, false);
    private final Bike bike8 = new Bike(Brand.TREK, Colour.WHITE, 40, false);
    private final Bike bike9 = new Bike(Brand.KROSS, Colour.BLUE, 10, true);
    private final Bike bike10 = new Bike(Brand.GIANT, Colour.RED, 20, false);

    private final List<Bike> bikeList;

    public BikeList() {
        this.bikeList = List.of(bike1, bike2, bike3, bike4, bike5, bike6, bike7, bike8, bike9, bike10);
    }

    public List<Bike> returnBikeList() {
        return bikeList;
    }
}
