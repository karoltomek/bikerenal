package com.kt.mvc.bikeRental.model;

public enum Colour {
    WHITE, BLACK, RED, BLUE, YELLOW, GRAY, GREEN
}
