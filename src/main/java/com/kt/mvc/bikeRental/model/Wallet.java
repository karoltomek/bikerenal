package com.kt.mvc.bikeRental.model;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Wallet {

    private double saldo;

    public Wallet(double saldo) {
        this.saldo = saldo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void addMoney(double money) {
        this.saldo += money;
    }

    void pay(double money) {
        this.saldo -= money;
    }

    @Override
    public String toString() {
        return "Saldo portfela wynosi: " + saldo;
    }

}
