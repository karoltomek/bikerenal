package com.kt.mvc.bikeRental.model;

import lombok.EqualsAndHashCode;

import java.util.LinkedList;
import java.util.List;

@EqualsAndHashCode
public class Client {

    private final double HOURS_FACTOR = 10000;
    private final Wallet wallet;
    private final List<Bike> loanedBikes = new LinkedList<>();

    public Client(Wallet wallet) {
        this.wallet = wallet;
    }

    public List<Bike> getLoanedBikes() {
        return loanedBikes;
    }

    public void addBike(Bike bike) {

        if (bike.isLoaned()) {

            System.out.println("Ten rower jest już wypożyczony");

        } else if (wallet.getSaldo() <= 0) {

            System.out.println("Doładuj portfel");

        } else {

            bike.setLoaned(true);
            bike.setStartTime(System.currentTimeMillis());
            loanedBikes.add(bike);
            System.out.println("Wypożyczono rower");
        }
    }

    public void removeBike(int bikeNumber) {

        int bikeNumberPosition = bikeNumber - 1;
        loanedBikes.get(bikeNumberPosition).setEndTime(System.currentTimeMillis());
        wallet.pay((loanedBikes.get(bikeNumberPosition).getEndTime() - loanedBikes.get(bikeNumberPosition).getStartTime())
                / HOURS_FACTOR
                * loanedBikes.get(bikeNumberPosition).getPricePerHour());
        loanedBikes.get(bikeNumberPosition).setLoaned(false);
        loanedBikes.remove(loanedBikes.get(bikeNumberPosition));
        System.out.println("Zwrócono rower");
    }

    @Override
    public String toString() {
        return "Client{" +
                "wallet=" + wallet +
                ", loanedBikes=" + loanedBikes +
                '}';
    }

}
