package com.kt.mvc.bikeRental.model;

public enum Brand {
    KROSS, GIANT, ROMET, SCOT, TREK
}
