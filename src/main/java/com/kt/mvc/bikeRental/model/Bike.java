package com.kt.mvc.bikeRental.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Bike {

    private final Brand brand;
    private final Colour colour;
    private final int pricePerHour;
    private boolean loaned;
    private long startTime;
    private long endTime;

    Bike(Brand brand, Colour colour, int pricePerHour, boolean loaned) {
        this.brand = brand;
        this.colour = colour;
        this.pricePerHour = pricePerHour;
        this.loaned = loaned;
    }

    boolean isLoaned() {
        return loaned;
    }

    @Override
    public String toString() {

        String isLoaned = loaned ? "TAK" : "NIE";

        return brand +
                " w kolorze " + colour +
                "\ncena za h: " + pricePerHour +
                "\nwypożyczony? " + isLoaned + "\n";
    }

}
