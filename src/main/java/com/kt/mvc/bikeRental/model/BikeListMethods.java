package com.kt.mvc.bikeRental.model;

import java.util.List;
import java.util.stream.Collectors;

 public class BikeListMethods {

     public static void showBikeList(List<Bike> bikeList) {
         for (int i = 0; i < bikeList.size(); i++) {
             System.out.println("Rower nr " + (i + 1));
             System.out.println(bikeList.get(i));
         }
     }

     public static List<Bike> bikeListByBrand(List<Bike> bikeList, String brand) {
         List<Bike> bikeListByBrand;
         bikeListByBrand = bikeList.stream()
                 .filter(p -> p.getBrand().toString().equals(brand)).collect(Collectors.toList());
         return bikeListByBrand;
     }

     public static List<Bike> bikeListByColour(List<Bike> bikeList, String colour) {
         List<Bike> bikeListByColour;
         bikeListByColour = bikeList.stream()
                 .filter(p -> p.getColour().toString().equals(colour)).collect(Collectors.toList());
         return bikeListByColour;
     }

     public static List<Bike> bikeListByPrice(List<Bike> bikeList, int price) {
         List<Bike> bikeListByPrice;
         bikeListByPrice = bikeList.stream()
                 .filter(p -> p.getPricePerHour() == price).collect(Collectors.toList());
         return bikeListByPrice;
     }

     public static List<Bike> bikeListByStatus(List<Bike> bikeList, boolean status) {
         List<Bike> bikeListByStatus;
         bikeListByStatus = bikeList.stream()
                 .filter(p -> p.isLoaned() == status).collect(Collectors.toList());
         return bikeListByStatus;
     }
}
