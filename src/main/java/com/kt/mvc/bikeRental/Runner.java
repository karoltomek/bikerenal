package com.kt.mvc.bikeRental;

import com.kt.mvc.bikeRental.controller.rentalController.RentalController;
import com.kt.mvc.bikeRental.controller.rentalController.RentalControllerInterface;

public class Runner {

    public static void main(String[] args) {

        RentalControllerInterface rentalController = new RentalController();
        rentalController.start();

    }
}