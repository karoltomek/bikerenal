package com.kt.mvc.bikeRental.controller.scannerController;

import java.util.Scanner;

public class ScannerController implements ScannerControllerInterface {

    @Override
    public String pickOption() {
        return new Scanner(System.in).next();
    }

    @Override
    public int nextInt() {
        return new Scanner(System.in).nextInt();
    }

    @Override
    public boolean nextBoolean() {
        return new Scanner(System.in).nextBoolean();
    }

    @Override
    public double nextDouble() {
        return new Scanner(System.in).nextDouble();
    }

}
