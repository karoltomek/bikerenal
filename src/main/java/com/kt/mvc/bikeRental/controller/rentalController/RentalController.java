package com.kt.mvc.bikeRental.controller.rentalController;

import com.kt.mvc.bikeRental.controller.scannerController.ScannerController;
import com.kt.mvc.bikeRental.controller.scannerController.ScannerControllerInterface;
import com.kt.mvc.bikeRental.model.BikeList;
import com.kt.mvc.bikeRental.model.BikeListMethods;
import com.kt.mvc.bikeRental.model.Client;
import com.kt.mvc.bikeRental.model.Wallet;

public class RentalController implements RentalControllerInterface {

    private final ScannerControllerInterface scannerController = new ScannerController();

    public void start() {
        BikeList bikeList = new BikeList();

        System.out.println("Wypożyczalnia rowerów");

        System.out.println("Twój portfel jest pusty, podaj kwotę doładowania: ");
        Wallet wallet = new Wallet(scannerController.nextDouble());
        System.out.println("Portfel doładowany, aktualne saldo wynosi: " + wallet.getSaldo());

        Client client = new Client(wallet);
        System.out.println("Aktualny stan bazy rowerów: ");
        BikeListMethods.showBikeList(bikeList.returnBikeList());

        boolean active = true;

        do {

            showMenu();
            int option = scannerController.nextInt();

            switch (option) {
                case 1:
                    System.out.println("Wybierz pozycję z listy: ");
                    int bicycleNumber = scannerController.nextInt();
                    client.addBike(bikeList.returnBikeList().get(bicycleNumber - 1));
                    break;
                case 2:
                    System.out.println("Wybierz pozycję z listy: ");
                    System.out.println(client.getLoanedBikes());
                    bicycleNumber = scannerController.nextInt();
                    client.removeBike(bicycleNumber);
                    break;
                case 3:
                    System.out.println("Wybierz po czym filtrować: (marka, kolor, cena, status, brak)");

                    String filterOption = scannerController.pickOption();

                    switch (filterOption) {
                        case "marka":
                            System.out.println("Podaj markę: ");
                            String chosenBrand = scannerController.pickOption();
                            BikeListMethods.showBikeList(BikeListMethods
                                    .bikeListByBrand(bikeList.returnBikeList(), chosenBrand));
                            break;
                        case "kolor":
                            System.out.println("Podaj kolor: ");
                            String chosenColour = scannerController.pickOption();
                            BikeListMethods.showBikeList(BikeListMethods
                                    .bikeListByColour(bikeList.returnBikeList(), chosenColour));
                            break;

                        case "cena":
                            System.out.println("Podaj cene: ");
                            int chosenPrice = scannerController.nextInt();
                            BikeListMethods.showBikeList(BikeListMethods
                                    .bikeListByPrice(bikeList.returnBikeList(), chosenPrice));
                            break;

                        case "status":
                            System.out.println("true - zajete; false - wolne");
                            boolean chosenStatus = scannerController.nextBoolean();
                            BikeListMethods.showBikeList(BikeListMethods
                                    .bikeListByStatus(bikeList.returnBikeList(), chosenStatus));
                            break;

                        case "brak":
                            BikeListMethods.showBikeList(bikeList.returnBikeList());
                            break;

                        default:
                            System.out.println("Nie wybrałeś żadnej opcji!");
                    }

                    break;
                case 4:
                    System.out.println("Wypożyczone rowery:");
                    BikeListMethods.showBikeList(client.getLoanedBikes());
                    break;
                case 5:
                    System.out.println("Podaj kwotę doładowania:");
                    int money = scannerController.nextInt();
                    wallet.addMoney(money);
                    break;
                case 6:
                    System.out.println(wallet.getSaldo());
                    break;
                case 7:
                    BikeListMethods.showBikeList(bikeList.returnBikeList());
                    break;
                case 8:
                    if (wallet.getSaldo() < 0) {
                        System.out.println("Masz ujemne saldo, doładuj konto!");
                    } else {
                        active = false;
                    }
                    break;
                default:
                    System.out.println("Nie wybrałeś żadnej opcji!");
            }
        } while (active);
    }

    void showMenu() {

        System.out.println("Wybierz opcję");
        System.out.println("1 - wypozycz rower" + "\n" +
                "2 - zwróć rower" + "\n" +
                "3 - filtruj listę" + "\n" +
                "4 - zobacz swoje wypożyczone rowery" + "\n" +
                "5 - doładuj portfel" + "\n" +
                "6 - sprawdź saldo" + "\n" +
                "7 - pokaż rowery" + "\n" +
                "8 - wyjdź z programu"
        );
    }

}
