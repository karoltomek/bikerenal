package com.kt.mvc.bikeRental.controller.scannerController;

public interface ScannerControllerInterface {

    String pickOption();

    int nextInt();

    boolean nextBoolean();

    double nextDouble();

}
